
APP_NAME = 'sitename prod'
    
DEBUG = False
LOG_FILE = './sitename.log'

# sqlalchemy configuration
SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_DATABASE_URI = 'sqlite:///database.sqlite'
SECRET_KEY = 'not so safe'
