import os

env = os.getenv('ENV', 'DEVELOPMENT')

from .common import *

if env=='PRODUCTION':
    from .production import *
elif env=='DEVELOPMENT':
    from .development import *
elif env=='TEST':
    from .test import *
else:
    raise ValueError("Environment of type '%s' is not supported"%env)