from application.database.group import group
from application import db, create_app
from application.database import User

app = create_app()

with app.app_context():
    db.drop_all()
    db.create_all()

    sylvan = User(email='sledeunf@gmail.com', username="HoshiyoSan", password='toto', groups=["user", "admin"])
    baptiste = User(email='bprieur@gmail.com', username="Batpok", password='toto', groups=["user"])
    db.session.add_all((sylvan, baptiste))
    db.session.commit()

    user = User.login('sledeunf@gmail.com', 'toto')
    print(user)
    print(user.groups)
    print(user, [group.name for group in user.groups])
