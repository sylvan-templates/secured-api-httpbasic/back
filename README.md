# Flask secured API (HTTPBasicAuth)

> Simple API with user authentication using HTTPBasicAuth.

## Installation

* Clone the project
* Create a virtualenv and use it
```
virtualenv env
source env/bin/activate
```
* Installed required modules
```
pip install -r requirements.txt
```
* You can edit configuration used for production, development, or testing env in `config/` folder.


# Run the project
```
python run.py
```
