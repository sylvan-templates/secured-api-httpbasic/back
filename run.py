from application import create_app, auth, logger, db
from application.resources import *
from application.database import User
import bjoern

app = create_app()

if __name__ == '__main__':
    bjoern.run(app, '0.0.0.0', 8000, reuse_port=True)
