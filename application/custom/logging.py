import logging
from config import LOG_FILE, DEBUG

class Logger:
    def __new__(self):
        logger = logging.getLogger(LOG_FILE)
        handler = logging.FileHandler(LOG_FILE)
        handler.setFormatter(logging.Formatter("%(asctime)s :::: %(levelname)s :::: %(message)s"))
        handler.setLevel(DEBUG)
        logger.setLevel(DEBUG)
        logger.addHandler(handler)
        return logger
