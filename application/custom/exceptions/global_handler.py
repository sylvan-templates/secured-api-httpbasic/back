from flask import jsonify
from config import DEBUG
from application import logger

def global_handler(error=None):
    error_message = str(error)
    if DEBUG:
        logger.error(error_message)
        raise error
    else:
        logger.error(error_message)
        return jsonify({
            "success": False,
            "message": "an error occured"
        })
