from application import mail
from flask import render_template
from flask_mail import Message

def send_email(recpt, topic, template, **context):
    try:
        if type(recpt)==str:
            recpt = [recpt]
        msg = Message(topic, recipients=recpt)
        msg.html = render_template(template, **context)
        mail.send(msg)
        return True
    except Exception as e:
        print(e)
        return False
