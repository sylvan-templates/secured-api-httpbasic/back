from flask import request, g, abort, render_template
from config import APP_NAME
from application import auth, Resource, db
from application.database import User, ResetToken, group
from application.custom.email import send_email
from flask_restplus import Namespace


api = Namespace("Account", "Account managing", path='/')


@api.route('/account/my')
class GetMyAccount(Resource):
    @auth.login_required
    def get(self):
        """
        Get current user's account
        """
        return User.Schema(many=False).dump(g.user).data


@api.route('/account/<int:id>')
class GetAccountById(Resource):
    @auth.login_required
    def get(self, id=None):
        """
        Get an account by id
        """
        user = User.query.filter_by(id=id).first()
        user_data = User.Schema(many=False).dump(user).data
        return user_data


@api.route('/account/<int:id>')
class ChangeAccountById(Resource):
    @auth.login_required
    def put(self, id=None, **params):
        """
        Modify user account given its id
        """
        if g.user.id==id or g.user.has_group("admin"):
            user = User.query.filter_by(id=id).first()
            if user:
                for key, val in params.items():
                    if key=="password":
                        return {
                            "success": False,
                            "message": "Not allowed to change user password with this method, please use /auth/password/reset instead."
                        }, 403
                    else:
                        setattr(user, key, val)
                db.session.commit()
            else:
                return {"message": "item not found"}, 404
        else:
            abort(403)

@api.route('/accounts')
class ListAccounts(Resource):
    @auth.login_required
    def get(self):
        """
        Get an account
        """
        filters = [getattr(User, key).ilike(val) for key, val in request.args.items()]
        users = User.query.filter(*filters).all()
        return User.Schema(many=True).dump(users).data

@api.route('/account/register')
class Register(Resource):
    def post(self, username:str, email:str, password:str):
        """
        Create an account.
        """
        user = User(
            username=username,
            email=email, 
            password=password
        )
        db.session.add(user)
        db.session.commit()

        user_data = User.Schema(many=False).dump(user).data
        return user_data


@api.route('/account/<int:id>')
class RemoveAccount(Resource):
    @auth.login_required
    def delete(self, id=None):
        """
        Remove user account by id.
        """
        if id==g.user.id or group.admin in g.user.groups:
            db.session.delete(g.user)
            db.session.commit()
            return "done"
        else:
            return abort(403)
