from flask_restplus import Api
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_httpauth import HTTPBasicAuth
from flask_mail import Mail
from flask import Blueprint

from config import APP_NAME
from application.custom.logging import Logger
from application.custom.exceptions import ExceptionsHandler

api = Api(version="0.0.1", title=APP_NAME, doc="/")
db = SQLAlchemy()
ma = Marshmallow()
auth = HTTPBasicAuth()
logger = Logger()
exc = ExceptionsHandler()
mail = Mail()
