import config
from flask import Flask
from .custom.resource import Resource, DBResource
from .components import api, db, ma, exc, logger, mail, auth


def create_app():
    app = Flask(config.APP_NAME, template_folder="application/templates")
    app.config.from_object(config)

    # flask restful api
    api.init_app(app)
    # sqlalchemy database orm
    db.init_app(app)
    # marshmallow object serializer
    ma.init_app(app)
    # custom exception handler
    exc.init_app(app)
    #
    mail.init_app(app)

    # needed for authentication logic
    from .database import user

    return app
